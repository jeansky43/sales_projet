import pandas as pd

df = pd.read_excel('/home/user/Projects/analysing/pivot-tables.xlsx', sheet_name='Sheet1')



df[['Product', 'Country', 'Category']] = df[['Product', 'Country', 'Category']].apply(lambda x: x.str.lower().str.strip())



# Fonction pour créer un tableau croisé dynamique (TCD) par pays
def create_tcd(list_pays, list_products):
    
    fichier = df[
        df['Country'].str.lower().isin(map(str.lower, list_pays)) &
        df['Product'].str.lower().isin(map(str.lower, list_products))
    ]
    
      
    tcd = pd.pivot_table(fichier, values='Amount', index='Product', aggfunc='count').rename(columns={'Amount': 'Count of Amount'})
    
    
    tcd.loc['Total result'] = tcd['Count of Amount'].sum()
    
    return tcd


selected_countries = ['France']
selected_products = ['Apple', 'Banana', 'Beans', 'Broccoli', 'Carrots', 'Mango', 'Orange']

# Appel de la fonction avec les pays et les produits spécifiés
sale = create_tcd(selected_countries, selected_products)

print(sale)

# Fonction pour créer un tableau croisé dynamique (TCD) 

get_all = lambda cats, countries, products: pd.pivot_table(
    df[
        (df['Category'].str.lower().isin(map(str.lower, cats))) & 
        (df['Country'].str.lower().isin(map(str.lower, countries))) &
        (df['Product'].str.lower().isin(map(str.lower, products)))
    ],
    values='Amount', index='Country', columns='Product',
    aggfunc='sum', margins=True, margins_name='Total'
)

# Définition des catégories, des pays et des produits à filtrer
categories = ['fruit', 'vegetables']
selected_countries = ['France', 'Canada']
selected_products = ['Apple', 'Banana', 'Beans', 'Broccoli', 'Carrots', 'Mango', 'Orange']

# Appel de la fonction avec les catégories, les pays et les produits spécifiés
result = get_all(categories, selected_countries, selected_products)

print(result)







