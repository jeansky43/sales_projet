import pandas as pd
import mysql.connector

# Connexion à la base de données MySQL
conn = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="sales"
)
cursor = conn.cursor()

# Charger la feuille Excel spécifiée
df = pd.read_excel('/home/user/Projects/analysing/pivot-tables.xlsx', sheet_name='Sheet1')

# Renommer les colonnes pour uniformiser les noms
df = df.rename(columns={'Category': 'Product_Category', 'Amount': 'Price', 'Date': 'Order_Date', 'Country': 'Country_Name'})

# Convertir les dates au format YYYY-MM-DD
df['Order_Date'] = pd.to_datetime(df['Order_Date']).dt.strftime('%Y-%m-%d')

# Supprimer les colonnes inutiles
df = df.drop(columns=['Order ID'])

# Requêtes SQL

# Insérer les données dans la table product:

for index, row in df.iterrows():    
    Product = row["Product"]
    Category = row["Product_Category"]
    cursor.execute("SELECT id_prod FROM product WHERE name = %s", (Product,))
    id_product = cursor.fetchall()
    if not id_product:
        sql2 = "INSERT INTO product (name, category) VALUES (%s, %s)"
        val2 = (Product, Category)
        cursor.execute(sql2, val2)        
        id_product = cursor.lastrowid
    else:
        id_product = id_product[0][0]
    
# Insérer les données dans la table country:
        
    Country = row['Country_Name']
    cursor.execute("SELECT id_pays FROM country WHERE nom = %s", (Country,))
    id_country = cursor.fetchall()
    if not id_country:
        sql = "INSERT INTO country (nom) VALUES (%s)"
        val = (Country,)
        cursor.execute(sql, val)
        id_country = cursor.lastrowid
    else:
        id_country = id_country[0][0]
    
# Insérer les données dans la table orders:
        
    Date = row['Order_Date']
    Amount = row['Price']
    sql3 = "INSERT INTO orders (date, amount, id_prod, id_pays) VALUES (%s, %s, %s, %s)"
    val3 = (Date, Amount, id_product, id_country)
    cursor.execute(sql3, val3)

conn.commit()

# Fermer la connexion
cursor.close()
conn.close()

print("Les données de la feuille spécifiée ont été normalisées et insérées dans la base de données avec succès.")
