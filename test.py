import pandas as pd
import mysql.connector

conn = mysql.connector.connect(
    host='localhost',
    user='John',
    password='docu',
    database='sales'
)


# Récupérer les données 

query_country = "SELECT * FROM country"
query_product = "SELECT * FROM product"
query_orders = "SELECT * FROM orders"

df_country = pd.read_sql(query_country, conn)
df_product = pd.read_sql(query_product, conn)
df_orders = pd.read_sql(query_orders, conn)

conn.close()

# Fusionner
df = pd.merge(df_orders, df_country, on='id_pays', how='left')
df = pd.merge(df, df_product, on='id_prod', how='left')

