import pandas as pd
import dash
from dash import dcc, html
from dash.dependencies import Input, Output

# Lecture fichier excel
df = pd.read_excel('/home/user/Projects/analysing/pivot-tables.xlsx', sheet_name='Sheet1')


df[['Product', 'Country', 'Category']] = df[['Product', 'Country', 'Category']].apply(lambda x: x.str.lower().str.strip())

# Fonction pour filtrer les données
def get_filtered_data(categories, countries, products):
    return df[
        (df['Category'].str.lower().isin(map(str.lower, categories))) & 
        (df['Country'].str.lower().isin(map(str.lower, countries))) &
        (df['Product'].str.lower().isin(map(str.lower, products)))
    ]

# Création de l'application Dash
app = dash.Dash(__name__)

# Mise en page
app.layout = html.Div([
    dcc.Dropdown(
        id='category-dropdown',
        options=[{'label': cat.title(), 'value': cat} for cat in df['Category'].unique()],
        multi=True,
        value=['fruit', 'vegetables'],
        placeholder="Select categories"
    ),
    dcc.Dropdown(
        id='country-dropdown',
        options=[{'label': country.title(), 'value': country} for country in df['Country'].unique()],
        multi=True,
        value=['France', 'Canada', 'Australia'],
        placeholder="Select countries"
    ),
    dcc.Dropdown(
        id='product-dropdown',
        options=[{'label': product.title(), 'value': product} for product in df['Product'].unique()],
        multi=True,
        value=['Apple', 'Banana', 'Beans', 'Broccoli', 'Carrots', 'Mango', 'Orange'],
        placeholder="Select products"
    ),
    dcc.Graph(id='histogram-graph')
])

# Mise à jour
@app.callback(
    Output('histogram-graph', 'figure'),
    [Input('category-dropdown', 'value'),
     Input('country-dropdown', 'value'),
     Input('product-dropdown', 'value')]
)
def update_histogram(categories, countries, products):
    filtered_data = get_filtered_data(categories, countries, products)
    pivot_table = pd.pivot_table(filtered_data, values='Amount', index='Country', columns='Product', aggfunc='sum', margins=True, margins_name='Total')
    return {
        'data': [{
            'type': 'bar',
            'x': pivot_table.columns,
            'y': pivot_table.loc['Total']
        }],
        'layout': {
            'title': 'Total Amount by Product',
            'xaxis': {'title': 'Product'},
            'yaxis': {'title': 'Amount'}
        }
    }

# Exécution 
if __name__ == '__main__':
    app.run_server(debug=True)
