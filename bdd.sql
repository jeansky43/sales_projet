DROP DATABASE IF EXISTS sales;

CREATE DATABASE sales;

USE sales;

GRANT ALL PRIVILEGES ON sales.* TO 'John'@'localhost';

CREATE TABLE `product`(
    `id_prod` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `category` ENUM('fruit', 'vegetables') NOT NULL
);
CREATE TABLE `country`(
    `id_pays` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `nom` VARCHAR(255) NOT NULL
);
CREATE TABLE `orders`(
    `id_order` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `date` DATETIME NOT NULL,
    `amount` INT NOT NULL,
    `id_prod` INT NOT NULL,
    `id_pays` INT NOT NULL,
    FOREIGN KEY (`id_prod`) REFERENCES `product`(`id_prod`),
    FOREIGN KEY (`id_pays`) REFERENCES `country`(`id_pays`)
);

